#include "../include/Calculations.h"
#define g 9.8
#define PI 3.14159265
#define deg * PI / 180.0


//few aerodynamic coefficient
struct MEDIUM{
    double P;
    double alpha;
    double X;
    double sinteta;
    double k;
    double teta;

    /*TO DO
        Do i really need this flag?
    */
    bool isCalculated;


    MEDIUM() {
        P = 0;
        alpha = 0;
        X = 0;
        sinteta = 0;
        k = 0;
        teta = 0;
        isCalculated = false;
    }
};

void calcMedium(MEDIUM &medium, PLANE &plane) {

    double Ro = 3.3e-09 * pow(plane.H, 2.0) + -1.1e-04 * plane.H + 1.2;

    medium.P = 160.0 * plane.H/1000.0 + 1716.0;

    medium.alpha = (plane.m * g - (-0.04 * Ro * pow(plane.V, 2.0))/2.0) / (medium.P/57.3  + 13 * Ro * pow(plane.V, 2.0) / 2.0);

    double Cy = 0.09 * medium.alpha - 0.04;

    double Cx = 0.0626 * pow(Cy, 2) -0.003 * Cy + 0.017;

    double V = (plane.V1 + plane.V2) / 2.0;

    medium.X = Cx * Ro * pow(V, 2)/2.0;

    medium.k = (plane.V2 - plane.V1) / (plane.H2 - plane.H1);

    medium.teta = ((medium.P - medium.X) * 57.3) / (plane.m * g);

    medium.sinteta = (medium.P * cos(medium.alpha deg) - medium.X) / (plane.m * (medium.k * plane.V + 9.8));

    medium.isCalculated = true;
}


/*TO DO
    this thing now does nothing, but we need verification while calculating time
    probably we are out of fly zone

double calcH(double V)
{
    double H = -3.66e-06 * pow(V, 2) + 0.01 * V + 1.0;
    return H;
}
*/

//calculates time of acceleration only thats why dV= V2-V1 and H -const
double SpeedGainTime(PLANE &plane) {
    MEDIUM medium;
    double  t = 0;

    calcMedium(medium, plane);
    if (medium.isCalculated == true)
        t = ((plane.V2 - plane.V1) * plane.m) / (medium.P * cos(medium.alpha deg) - medium.X);

    return t;
}

//calculates time of clibing thats why dH= H2 - H1 V = const
//this quation is similar with type when plane claimbs and gining speed at the same time
double ClimbTime(PLANE &plane) {
    MEDIUM medium;;
    double  t = 0;

    calcMedium(medium, plane);
    if (medium.isCalculated == true)
        t = 57.3 * (plane.H2 - plane.H1) / (plane.V * medium.teta);
    return t;
}

double SpeedClimbTime(PLANE &plane) {
    MEDIUM medium;
    double  t = 0;

    calcMedium(medium, plane);
    if (medium.isCalculated == true)
        t = (log(plane.V2 / plane.V1)) / (medium.k * medium.sinteta);
                /*
                there are two ways how to calculate time in this case they are similar
                (log(plane.V2 / plane.V1)) / (medium.k * medium.sinteta);
                57.3 * (plane.H2 - plane.H1) / (plane.V * medium.teta);
                */

    return t;
}
