#include "../include/Matrix.h"

//creates a speed gain time matrix
void CreateHorizontalMatrix(TMATRIXD &matrix, PLANE &plane) {

    plane.V = plane.V2 - plane.dV;
    plane.H = plane.H2;
    QVector<double> raw;

    for (int i = 0; i < plane.dy + 1; i++) {
        matrix.append(raw);
        for (int j = 0; j < plane.dx; j++) {
            matrix[i].append(SpeedGainTime(plane));
            plane.V -= plane.dV;
        }
        plane.H -= plane.dH;
        plane.V = plane.V2;
    }
    plane.V = 0;
    plane.H = 0;
}

//creates climb time matrix
void CreateVerticalMatrix(TMATRIXD &matrix, PLANE &plane) {
    plane.H = plane.H2 - plane.dH;
    plane.V = plane.V2;
    QVector<double> raw;

    for (int i = 0; i < plane.dy; i++) {
        matrix.append(raw);
        for (int j = 0; j < plane.dx + 1; j++) {
            matrix[i].append(ClimbTime(plane));
            plane.V -= plane.dV;
        }
        plane.H -= plane.dH;
        plane.V = plane.V2;
    }
    plane.V = 0;
    plane.H = 0;
}

//creates time Matrix of acceleration and lifting thats why V = V2 H = H2
void CreateVHMatrix(TMATRIXD &matrix, PLANE &plane) {
    plane.H = plane.H2 - plane.dH;
    plane.V = plane.V2 - plane.dV;
    QVector<double> raw;
    for (int i = 0; i < plane.dx; i++) {
        matrix.append(raw);
        for (int j = 0; j < plane.dy; j++) {
            matrix[i].append(SpeedClimbTime(plane));
            plane.V -= plane.dV;
        }
        plane.H -= plane.dH;
        plane.V = plane.V2;
    }
    plane.V = 0;
    plane.H = 0;
}
